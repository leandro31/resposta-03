package br.com.book.provasjpa.repository;

import br.com.book.provasjpa.model.Book;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BookRepository extends JpaRepository <Book, Long> {
    List<Book> findByBookname(String nameBook);

    void delete(Long id);
}
