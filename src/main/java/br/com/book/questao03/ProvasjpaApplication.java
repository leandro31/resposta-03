package br.com.book.provasjpa;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProvasjpaApplication {

    public static void main(String[] args) {
        SpringApplication.run(provasjpaApplication.class, args);
    }

}
